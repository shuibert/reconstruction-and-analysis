# Overview

This is a guide to EUtelescope reconstruction and anaylsis in Tbmon2.

 0. [Description](#sec_intro)

 1. [Installation](#sec_install)

 2. [Configuration in EUtelescope](#sec_configEU)

 3. [Configuration in TBmon2](#sec_configTB)

 4. [Plots](#sec_plots)
 
 5. [DAF/DUT Align and GBL](#sec_daf)


<a name="sec_intro"></a>
# 0. Introduction

This page serves as a guide to EUtelescope and TBmon2 by:

1) Having useful links connecting instructions allready written:

2) Having new information regarding Eutelescope and TBmon2. 
 
## 1). Useful Links

**Download and install:**

 Norway ITk Testbeam: https://gitlab.cern.ch/NorwayItkPix
 
 Mareike's TBmon2: https://gitlab.cern.ch/mweers/tbmon2

**Helpful person:**

 Tobias's gitlab: https://gitlab.cern.ch/tofitsch/testbeamreco

**Description and help:**

 Twiki on Reconstruction: https://twiki.cern.ch/twiki/bin/view/Atlas/ITkPixelTestBeamReconstruction

**Manual or Papers:**

https://arxiv.org/pdf/1603.09669.pdf
 
 
<a name="sec_install"></a>
# 1. Installation

## Install Eutelescope
 
To install Eutelescope locally on ubuntu 18 (version by Norway-ITk-Group) check the file:

EUTELESCOPE INSTALL UBUNTU 18.04 LTS

To install Eutelescope on lxplus (ATLAS-ITk version) see gitlab from Tobias:

https://gitlab.cern.ch/tofitsch/testbeamreco

This is also a very nice site for info and files regarding EUtelescope and TBmon2.

<a name="sec_configEU"></a>
# 2. Configuration in EUtelscope





 


